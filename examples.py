import collections
import glob
import os

import math

from model import CROP_SIZE
from rgb_lab import *

Examples = collections.namedtuple("Examples", "paths, input_placeholder, inputs, targets, count, steps_per_epoch")

def preprocess(image):
    with tf.name_scope("preprocess"):
        # [0, 1] => [-1, 1]
        return image * 2 - 1

def preprocess_lab(lab):
    with tf.name_scope("preprocess_lab"):
        L_chan, a_chan, b_chan = tf.unstack(lab, axis=2)
        # L_chan: black and white with input range [0, 100]
        # a_chan/b_chan: color channels with input range ~[-110, 110], not exact
        # [0, 100] => [-1, 1],  ~[-110, 110] => [-1, 1]
        return [L_chan / 50 - 1, a_chan / 110, b_chan / 110]

def load_examples_from_fs(a):
    if a.input_dir is None or not os.path.exists(a.input_dir):
        raise Exception("input_dir does not exist")

    input_paths = glob.glob(os.path.join(a.input_dir, "*.jpg"))
    decode = tf.image.decode_jpeg
    if len(input_paths) == 0:
        input_paths = glob.glob(os.path.join(a.input_dir, "*.png"))
        decode = tf.image.decode_png

    if len(input_paths) == 0:
        raise Exception("input_dir contains no image files")

    def get_name(path):
        name, _ = os.path.splitext(os.path.basename(path))
        return name

    # if the image names are numbers, sort by the value rather than asciibetically
    # having sorted inputs means that the outputs are sorted in test mode
    if all(get_name(path).isdigit() for path in input_paths):
        input_paths = sorted(input_paths, key=lambda path: int(get_name(path)))
    else:
        input_paths = sorted(input_paths)

    with tf.name_scope("load_images"):
        path_queue = tf.train.string_input_producer(input_paths, shuffle=a.mode == "train")
        reader = tf.WholeFileReader()
        paths, contents = reader.read(path_queue)
        raw_input = decode(contents, channels=4)
        raw_input = tf.image.convert_image_dtype(raw_input, dtype=tf.float32)

        raw_input.set_shape([None, None, 4])

        # break apart image pair and move to range [-1, 1]
        width = tf.shape(raw_input)[1]  # [height, width, channels]

        # images in order: circles (rgb=color, alpha=depth), obj (rgb=color, alpha=depth), obj normals (alpha unused)
        part_width = width // 3
        circles = raw_input[:, 0:part_width, :]
        obj = raw_input[:, part_width:2 * part_width, :]
        obj_norm = raw_input[:, 2 * part_width:, 0:3]
        obj_norm.set_shape([None, None, 3])

        if a.lab_colorization:
            circle_color = circles[:, :, 0:3]
            circle_depth = circles[:, :, 3:4]
            obj_color = obj[:, :, 0:3]
            obj_depth = obj[:, :, 3:4]

            circle_lab = rgb_to_lab(circle_color)
            circle_L, circle_a, circle_b = preprocess_lab(circle_lab)
            circle_lab_stack = tf.stack([circle_L, circle_a, circle_b], 2)

            obj_lab = rgb_to_lab(obj_color)
            obj_L, obj_a, obj_b = preprocess_lab(obj_lab)
            obj_lab_stack = tf.stack([obj_L, obj_a, obj_b], 2)

            concatenated_input = tf.concat([circle_lab_stack, preprocess(circle_depth[:, :, 0:1])], 2)
            concatenated_output = tf.concat([obj_lab_stack, preprocess(obj_depth[:, :, 0:1]), preprocess(obj_norm)], 2)

            a_images = concatenated_input
            b_images = concatenated_output
        else:
            concatenated_input = circles
            concatenated_output = tf.concat([obj, obj_norm], 2)

            a_images = preprocess(concatenated_input)
            b_images = preprocess(concatenated_output)

    inputs, targets = [a_images, b_images]

    def transform(image):
        r = image
        r = tf.image.resize_images(r, [CROP_SIZE, CROP_SIZE], method=tf.image.ResizeMethod.AREA)
        return r

    with tf.name_scope("input_images"):
        input_images = transform(inputs)

    with tf.name_scope("target_images"):
        target_images = transform(targets)

    paths_batch, inputs_batch, targets_batch = tf.train.batch([paths, input_images, target_images], batch_size=a.batch_size)
    steps_per_epoch = int(math.ceil(len(input_paths) / a.batch_size))

    return Examples(
        paths=paths_batch,
        input_placeholder=None,
        inputs=inputs_batch,
        targets=targets_batch,
        count=len(input_paths),
        steps_per_epoch=steps_per_epoch,
    )


def load_examples_placeholder(a):
    with tf.name_scope("load_images"):
        input_placeholder = tf.placeholder(tf.uint8, shape=[CROP_SIZE, CROP_SIZE, 4], name='input_placeholder')

        scaled_input_placeholder = tf.cast(input_placeholder, tf.float32)
        scaled_input_placeholder = tf.divide(scaled_input_placeholder, 255.0)

        if a.lab_colorization:
            circle_lab = rgb_to_lab(scaled_input_placeholder[:, :, 0:3])
            circle_L, circle_a, circle_b = preprocess_lab(circle_lab)
            circle_lab_stack = tf.stack([circle_L, circle_a, circle_b], 2)

            concatenated_input = tf.concat([circle_lab_stack, preprocess(scaled_input_placeholder[:, :, 3:4])], 2)
            a_images = concatenated_input
        else:
            a_images = preprocess(scaled_input_placeholder)

        b_images = tf.ones(shape=[CROP_SIZE, CROP_SIZE, 7], dtype=tf.float32, name='no-target')

    inputs, targets = [a_images, b_images]

    def transform(image):
        # area produces a nice downscaling, but does nearest neighbor for upscaling
        # assume we're going to be doing downscaling here
        return tf.image.resize_images(image, [a.scale_size, a.scale_size], method=tf.image.ResizeMethod.AREA)

    with tf.name_scope("input_images"):
        input_images = transform(inputs)

    with tf.name_scope("target_images"):
        target_images = transform(targets)

    # inputs_batch, targets_batch = tf.train.batch([input_images, target_images], batch_size=a.batch_size)
    # inputs_batch, targets_batch = tf.expand_dims([input_images, target_images], 0)
    inputs_batch = tf.expand_dims(input_images, 0)
    targets_batch = tf.expand_dims(target_images, 0)
    steps_per_epoch = 1

    return Examples(
        paths=None,
        input_placeholder=input_placeholder,
        inputs=inputs_batch,
        targets=targets_batch,
        count=1,
        steps_per_epoch=steps_per_epoch,
    )
