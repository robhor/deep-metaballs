#version 120

uniform vec3 color;
uniform float radius;
uniform int outputType;

varying vec2 uv;
varying float depth;

void mainImage(out vec4 fragColor, in vec2 fragCoord)
{
    if (length(fragCoord - 0.5) > 0.45 && length(fragCoord - 0.5) < 0.5) {
        fragColor = vec4(color, 1.0);
        float centerDist = (0.5 - length(fragCoord - 0.5)) * 2;
        gl_FragDepth = depth;

        if (outputType == 1) {
            fragColor.rgb = vec3(gl_FragDepth);
        }
    } else {
        discard;
    }
}

void main() {
    mainImage(gl_FragColor, uv);
}
